﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Funktionsparser
{
    public class FunktionXY : Funktion
    {
        private FunktionXY(string definition)
            : base(definition)
        { }

        public static FunktionXY Parse(string definition)
        {
            definition = definition.ToLower().Replace(" ", "");
            FunktionXY fn = new FunktionXY(definition);
            fn.Variablen = new char[] { 'x', 'y' };
            fn.Baum = GetBaum(
                GetUPNTokens(
                    GetTokens(definition, fn.Variablen)));

            return fn;
        }

        public double GetAt(double x, double y)
        {
            return Baum.GetAt(new Paar<char, double>('x', x), new Paar<char, double>('y', y));
        }
    }
}
