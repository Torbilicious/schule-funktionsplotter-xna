﻿using System;
using System.Collections.Generic;

namespace Funktionsparser
{
    class SubFunktion2 : SubFunktion
    {
        private static Dictionary<string, Func<double, double>> Funktionen = new Dictionary<string, Func<double, double>>() {
            {"sind", (d => Math.Sin(d*180/Math.PI))},
            {"sin", Math.Sin},
            {"asin", Math.Asin},
            {"arcsin", Math.Asin},
            {"sinh", Math.Sinh},
            {"cos", Math.Cos},
            {"cosd", (d => Math.Cos(d*180/Math.PI))},
            {"acos", Math.Acos},
            {"arccos", Math.Acos},
            {"cosh", Math.Cosh},
            {"tand", (d => Math.Tan(d*180/Math.PI))},
            {"tan", Math.Tan},
            {"atan", Math.Atan},
            {"arctan", Math.Atan},
            {"tanh", Math.Tanh},
            {"sign", (d=>(double)Math.Sign(d))},
            {"sqrt", Math.Sqrt},
            {"floor", Math.Floor},
            {"ceil", Math.Ceiling},
            {"ceiling", Math.Ceiling},
            {"round", Math.Round},
            {"abs", Math.Abs},
            {"ln", Math.Log},
            {"log10", Math.Log10}
        };

        public Func<double, double> Funktion;
        public string Name;

        public SubFunktion2(string name) {
            Name = name;
            if (Funktionen.ContainsKey(name)) {
                Funktion = Funktionen[name];
            } else {
                throw new Exception("Funktion " + name + " wurde nicht erkannt.");
            }
        }

        public static bool Definiert(string fn) {
            return Funktionen.ContainsKey(fn);
        }

        public override string ToString() {
            return Name;
        }
    }
}
