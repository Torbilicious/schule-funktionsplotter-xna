﻿using System.Collections.Generic;
using System;
namespace Funktionsparser
{
    class Funktionsbaum : Baum<Funktionsteil>
    {
        public double GetAt(params Paar<char, double>[] variablen)
        {
            Dictionary<char, double> vars = new Dictionary<char, double>();
            foreach (var v in variablen)
                vars.Add(v.Schluessel, v.Wert);
            return GetAt(vars, Wurzel);
        }

        private double GetAt(Dictionary<char, double> variablen, Ast<Funktionsteil> wurzel)
        {
            Funktionsteil f = wurzel.Wert;
            if (f is Zahl)
                return ((Zahl)f).Wert;
            else if (f is Variable)
                return variablen[((Variable)f).Zeichen];
            else if (f is SubFunktion2)
                return ((SubFunktion2)f).Funktion(GetAt(variablen, wurzel.Links));
            else if (f is SubFunktion3)
                return ((SubFunktion3)f).Funktion(GetAt(variablen, wurzel.Links), GetAt(variablen, wurzel.Rechts));
            else if(f is Operator)
                return ((Operator)f).Operation(GetAt(variablen, wurzel.Links), GetAt(variablen, wurzel.Rechts));
            throw new Exception("Nix Rekursion");
        }
    }
}
