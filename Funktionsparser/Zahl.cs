﻿namespace Funktionsparser
{
    class Zahl : Funktionsteil
    {
        public double Wert;
        public Zahl(double d) {
            Wert = d;
        }

        public override string ToString() {
            return Wert + "";
        }
    }
}
