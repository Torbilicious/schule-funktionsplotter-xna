﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Funktionsparser
{
    public class FunktionX : Funktion
    {
        private FunktionX(string definition)
            : base(definition)
        { }

        public static FunktionX Parse(string definition)
        {
            definition = definition.ToLower().Replace(" ", "");
            FunktionX fn = new FunktionX(definition);
            fn.Variablen = new char[] {'x'};
            fn.Baum = GetBaum(
                GetUPNTokens(
                    GetTokens(definition, fn.Variablen)));

            return fn;
        }

        public double GetAt(double x)
        {
            return Baum.GetAt(new Paar<char, double>('x', x));
        }
    }
}
