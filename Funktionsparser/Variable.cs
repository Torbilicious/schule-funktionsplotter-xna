﻿namespace Funktionsparser
{
    class Variable : Funktionsteil
    {
        public char Zeichen;
        public Variable(char z) {
            Zeichen = z;
        }
        public override string ToString() {
            return Zeichen + "";
        }
    }
}
