﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Funktionsparser
{
    public class Funktion
    {
        private static Dictionary<string, double> Konstanten;

        internal Funktionsbaum Baum;
        public string Definition;
        protected char[] Variablen;

        static Funktion()
        {
            Konstanten = new Dictionary<string, double>() 
            { 
                {"e", Math.E},
                {"pi", Math.PI}
            };
        }

        protected Funktion(string definition)
        {
            Baum = new Funktionsbaum();
            Definition = definition;
        }

        private Funktion(Funktionsbaum baum, char[] variablen)
        {
            Baum = baum;
            Variablen = variablen;
        }

        internal static Funktionsbaum GetBaum(List<Funktionsteil> tokens)
        {
            Funktionsbaum result = new Funktionsbaum();
            List<object> list = tokens.Cast<object>().ToList();

            while (list.Count > 1)
            {
                int index = -1;
                object item;
                do
                {
                    index++;
                    item = list[index];
                }
                while (!(item is SubFunktion || item is Operator));

                Funktionsast ast = new Funktionsast((Funktionsteil)item);
                int insertIndex = index - 1,
                    removeCount = 2;
                if (item is SubFunktion2)
                    ast.AddLinks(list[index - 1]);
                else
                {
                    ast.AddLinksRechts(list[index - 2], list[index - 1]);
                    insertIndex -= 1;
                    removeCount++;
                }
                list.RemoveRange(insertIndex, removeCount);
                list.Insert(insertIndex, ast);
            }
            if (list[0] is Funktionsast)
            {
                result.Wurzel = (Funktionsast)list[0];
            }
            else
            {
                result.Wurzel = new Funktionsast((Funktionsteil)list[0]);
            }

            return result;
        }

        internal static List<Funktionsteil> GetUPNTokens(List<Funktionsteil> tokens)
        {
            List<Funktionsteil> result = new List<Funktionsteil>();
            Stack<Funktionsteil> stack = new Stack<Funktionsteil>();

            /////////////////////////////////////
            // Shunting Yard Algorithmus hier! //
            /////////////////////////////////////
            #region SYA
            foreach (Funktionsteil cur in tokens)
            {
                if (cur is Zahl || cur is Variable)
                    result.Add(cur);
                else if (cur is SubFunktion)
                    stack.Push(cur);
                else if (cur is Argumenttrennzeichen)
                    while (stack.Peek().ToString() != "(")
                        result.Add(stack.Pop());
                else if (cur is Operator)
                {
                    while (stack.Count > 0 && stack.Peek() is Operator &&
                        (((cur as Operator).Praezedenz <= (stack.Peek() as Operator).Praezedenz && cur.ToString() != "^")
                        || (cur as Operator).Praezedenz < (stack.Peek() as Operator).Praezedenz))
                        result.Add(stack.Pop());
                    stack.Push(cur);
                }
                else if (cur is Klammer && (cur as Klammer).Oeffnend)
                {
                    stack.Push(cur);
                }
                else if (cur is Klammer && !((Klammer)cur).Oeffnend)
                {
                    if (stack.Count > 0)
                    {
                        while (stack.Count > 0 && stack.Peek().ToString() != "(")
                            result.Add(stack.Pop());
                        stack.Pop();
                    }
                    if (stack.Count > 0 && stack.Peek() is SubFunktion)
                        result.Add(stack.Pop());
                }
            }
            while (stack.Count > 0)
            {
                result.Add(stack.Pop());
            }
            #endregion

            return result;
        }

        internal static List<Funktionsteil> GetTokens(string definition, char[] variablen)
        {
            List<string> tokens = new List<string>();

            string cur = "";

            double d;
            foreach (char c in definition)
            {
                bool op;
                //falls c Operator ist
                if ((op = Operator.Definiert(c)) || c == '(' || c == ')' || c == Argumenttrennzeichen.Zeichen)
                {
                    bool zahl = double.TryParse(cur, out d);
                    //falls cur und tokens leer
                    if (cur.Length == 0 && op && (tokens.Count == 0 || tokens.Last() == "("))
                    {
                        tokens.Add("0");
                    }
                    //falls in cur Zahl, Variable oder SubFunktion
                    else if (zahl || (cur.Length == 1 && variablen.Contains(cur[0])) ||
                        (c == '(' && (SubFunktion2.Definiert(cur) || SubFunktion3.Definiert(cur))))
                    {
                        tokens.Add(cur);
                        cur = "";
                    }
                    else if (Konstanten.ContainsKey(cur))
                    {
                        tokens.Add(Konstanten[cur] + "");
                        cur = "";
                    }
                    else if (cur.Length > 0)
                    {
                        throw new Exception("Zeichenkette " + definition + " wurde nicht erkannt.");
                    }
                    tokens.Add(c + "");
                }
                else
                {
                    cur += c;
                }
            }
            if (cur.Length > 0 && (double.TryParse(cur, out d) || (cur.Length == 1 && variablen.Contains(cur[0]))))
            {
                tokens.Add(cur);
            }
            else if (Konstanten.ContainsKey(cur))
            {
                tokens.Add(Konstanten[cur] + "");
            }
            else if (cur.Length > 0)
            {
                throw new Exception("Zeichenkette " + definition + " wurde nicht erkannt.");
            }

            return GetFunktionsteile(tokens);
        }

        internal static List<Funktionsteil> GetFunktionsteile(List<string> tokens)
        {
            List<Funktionsteil> teile = new List<Funktionsteil>();
            double d;
            foreach (string token in tokens)
            {
                if (double.TryParse(token, out d))
                {
                    teile.Add(new Zahl(d));
                }
                else if (token.Length == 1 && Operator.Definiert(token[0]))
                {
                    teile.Add(new Operator(token[0]));
                }
                else if (token.Length == 1 && Klammer.Definiert(token[0]))
                {
                    teile.Add(new Klammer(token[0]));
                }
                else if (SubFunktion2.Definiert(token))
                {
                    teile.Add(new SubFunktion2(token));
                }
                else if (SubFunktion3.Definiert(token))
                {
                    teile.Add(new SubFunktion3(token));
                }
                else if (token.Length == 1 && token[0] == Argumenttrennzeichen.Zeichen)
                {
                    teile.Add(new Argumenttrennzeichen());
                }
                else
                {
                    teile.Add(new Variable(token[0]));
                }
            }
            return teile;
        }

        public Funktion Ableiten(char variable)
        { 
            Funktionsbaum f = new Funktionsbaum();
            f.Wurzel = Ableiten((Funktionsast)Baum.Wurzel, variable);
            return new Funktion(f, new char[] {variable});
                
        }

        private static Funktionsast Ableiten(Funktionsast wurzel, char variable)
        {
            if (wurzel.Links == null && wurzel.Rechts == null)
                return wurzel;
            if (wurzel.Wert.ToString() == "+" || wurzel.Wert.ToString() == "-")
            {
                Funktionsast f = new Funktionsast(new Operator(wurzel.Wert.ToString()[0]));
                f.AddLinksRechts(Funktion.Ableiten((Funktionsast)wurzel.Links, variable), Funktion.Ableiten((Funktionsast)wurzel.Rechts, variable));
                return f;
            }
            return null;
        }
    }
}
