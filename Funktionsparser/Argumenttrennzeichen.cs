﻿namespace Funktionsparser
{
    class Argumenttrennzeichen : Funktionsteil
    {
        public static readonly char Zeichen = ';';

        public override string ToString() {
            return ";";
        }
    }
}
