﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Funktionsparser
{
    class Ast<T>
    {
        public T Wert;
        public Ast<T> Links, Rechts;
        public Ast(T wert) {
            Wert = wert;
        }

        public void AddLinks(object obj)
        {
            if (obj is Ast<T>)
                Links = (Ast<T>)obj;
            else if (obj is T)
                Links = new Ast<T>((T)obj);
            else
                throw new Exception("Geht nich :( links");
        }

        public void AddRechts(object obj)
        {
            if (obj is Ast<T>)
                Rechts = (Ast<T>)obj;
            else if (obj is T)
                Rechts = new Ast<T>((T)obj);
            else
                throw new Exception("Geht nich :( rechts");
        }

        public void AddLinksRechts(object links, object rechts)
        {
            AddLinks(links);
            AddRechts(rechts);
        }
    }
}
