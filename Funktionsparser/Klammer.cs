﻿namespace Funktionsparser
{
    class Klammer : Funktionsteil
    {
        public bool Oeffnend;
        public Klammer(char c) {
            Oeffnend = c == '(';
        }

        public static bool Definiert(char c) {
            return c == '(' || c == ')';
        }

        public override string ToString() {
            return Oeffnend ? "(" : ")";
        }
    }
}
