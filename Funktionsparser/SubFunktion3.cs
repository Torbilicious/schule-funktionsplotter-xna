﻿using System;
using System.Collections.Generic;

namespace Funktionsparser
{
    class SubFunktion3 : SubFunktion
    {
        private static Dictionary<string, Func<double, double, double>> Funktionen = new Dictionary<string, Func<double, double, double>>() {
            {"max", Math.Max},
            {"min", Math.Min},
            {"atan2", Math.Atan2},
            {"arctan2", Math.Atan2},
            {"log", Math.Log}
        };

        public Func<double, double, double> Funktion;
        public string Name;

        public SubFunktion3(string name) {
            Name = name;
            if (Funktionen.ContainsKey(name)) {
                Funktion = Funktionen[name];
            } else {
                throw new Exception("Funktion " + name + " wurde nicht erkannt.");
            }
        }

        public static bool Definiert(string fn) {
            return Funktionen.ContainsKey(fn);
        }

        public override string ToString() {
            return Name;
        }
    }
}
