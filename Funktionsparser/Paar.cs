﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Funktionsparser
{
    class Paar<T, Y>
    {
        public T Schluessel;
        public Y Wert;

        public Paar(T schluessel, Y wert) {
            Schluessel = schluessel;
            Wert = wert;
        }
    }
}
