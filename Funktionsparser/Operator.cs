﻿using System.Collections.Generic;
using System;
using System.Linq;

namespace Funktionsparser
{
    class Operator : Funktionsteil
    {
        private static Dictionary<char, Func<double, double, double>> Operationen = new Dictionary<char, Func<double, double, double>>() {
            {'+', ((a, b) => a + b)},
            {'-', ((a, b) => a - b)},
            {'*', ((a, b) => a * b)},
            {'/', ((a, b) => a / b)},
            {'%', ((a, b) => a % b)},
            {'^', ((a, b) => Math.Pow(a, b))}
        };

        public Func<double, double, double> Operation;
        public char Zeichen;
        public int Praezedenz {
            get {
                return "+-".ToCharArray().Contains(Zeichen) ? 0 :
                    "*/%".ToCharArray().Contains(Zeichen) ? 1 :
                    Zeichen == '^' ? 2 : -1;
            }
        }
        public Operator(char z) {
            Zeichen = z;
            Operation = Operationen[z];
        }

        public static bool Definiert(char c) {
            return Operationen.ContainsKey(c);
        }

        public override string ToString() {
            return Zeichen + "";
        }
    }
}
