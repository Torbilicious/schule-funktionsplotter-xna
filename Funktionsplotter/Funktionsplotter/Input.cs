﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;
using Forms = System.Windows.Forms;
using System.Diagnostics;
using Microsoft.Xna.Framework.Graphics;

namespace Funktionsplotter
{
    public delegate void ExitHandler();
    static class Input
    {
        const float RotationFactor = 0.001f;

        public static Vector3 Rotation;
        public static float Scale { get { return _Scale >= 1 ? (float)Math.Sqrt(_Scale) : 1f / Math.Abs(_Scale - 1); } }
        private static int _Scale;
        public static Forms.Form Form;
        public static event ExitHandler Exit;
        public static FillMode FillMode = FillMode.WireFrame;

        private static void InfiniteMouseMovement(MouseState mouse)
        {
            Point mousePos = mouse.Position(),
                newMousePos = mousePos;
            if (mousePos.X <= 0)
                newMousePos.X = Form.Width + Form.Left - 1;
            else if (mousePos.X >= Form.Width + Form.Left - 1)
                newMousePos.X = Form.Left + 1;
            if (mousePos.Y <= 0)
                newMousePos.Y = Form.Height + Form.Top - 1;
            else if (mousePos.Y >= Form.Height + Form.Top - 1)
                newMousePos.Y = Form.Top + 1;
            if (mousePos != newMousePos)
                mouse.SetPosition(newMousePos);
        }

        public static void HandleMouseInput()
        {
            MouseState mouse = Mouse.GetState();

            Point mouseMovement = mouse.TrackMovement();
            if (mouse.IsButtonDown(0))
            {
                Rotation.Z += RotationFactor * mouseMovement.X;
                Rotation.X += RotationFactor * mouseMovement.Y;
            }
            int scroll = mouse.ScrollWheelValue / 120;
            Debug.WriteLine(scroll);
            _Scale = scroll;
            InfiniteMouseMovement(mouse);

            mouse.Update();
        }

        public static void HandleKeyboardInput()
        {
            KeyboardState keyboard = Keyboard.GetState();

            //close game with Escape
            if (keyboard.IsKeyDown(Keys.Escape))
                OnExit();

            if (keyboard.IsKeyPressed(Keys.F1))
                FillMode = FillMode.Solid;
            else if (keyboard.IsKeyPressed(Keys.F2))
                FillMode = FillMode.WireFrame;

            keyboard.Update();
        }

        private static void OnExit()
        {
            if (Exit != null)
                Exit();
        }
    }
}
