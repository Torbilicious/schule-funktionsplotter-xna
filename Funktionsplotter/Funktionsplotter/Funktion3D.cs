﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Funktionsparser;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Funktionsplotter
{
    class Funktion3D
    {
        public FunktionXY Funktion;
        private VertexPositionColor[] Vertizes;
        public short[] Indizes;

        public Funktion3D(string def)
        {
            Funktion = FunktionXY.Parse(def);
        }

        public void Generate(double xMin, double xMax, double yMin, double yMax, int vertizesReiheSpalte, Color color)
        {
            double xStep = (xMax - xMin) / vertizesReiheSpalte,
                yStep = (yMax - yMin) / vertizesReiheSpalte;
            Vertizes = new VertexPositionColor[vertizesReiheSpalte * vertizesReiheSpalte];
            Indizes = new short[Vertizes.Length * 6];
            int c = 0;
            for (int yf = 0; yf < vertizesReiheSpalte; yf++)
            {
                for (int xf = 0; xf < vertizesReiheSpalte; xf++)
                {
                    double x = xMin + xStep * xf,
                        y = yMin + yStep * yf,
                        z = Funktion.GetAt(x, y);
                    int i = xf + yf * vertizesReiheSpalte;
                    Vertizes[i].Position = new Vector3((float)x, (float)y, (float)z);

                    Vertizes[i].Color = color;
                    if (xf > 0 && yf > 0)
                    {
                        Indizes[c++] = (short)(xf - 1 + (yf - 1) * vertizesReiheSpalte);
                        Indizes[c++] = (short)(xf + (yf - 1) * vertizesReiheSpalte);
                        Indizes[c++] = (short)(xf - 1 + yf * vertizesReiheSpalte);

                        Indizes[c++] = (short)(xf + (yf - 1) * vertizesReiheSpalte);
                        Indizes[c++] = (short)(xf - 1 + yf * vertizesReiheSpalte);
                        Indizes[c++] = (short)(xf + yf * vertizesReiheSpalte);
                    }
                }
            }
        }

        public void Draw(GraphicsDevice device)
        {
            device.DrawUserIndexedPrimitives(PrimitiveType.TriangleList, Vertizes, 0, Vertizes.Length, Indizes, 0, Indizes.Length / 3);
        }
    }
}
