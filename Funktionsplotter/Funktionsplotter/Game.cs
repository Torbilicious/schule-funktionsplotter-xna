using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Forms = System.Windows.Forms;

namespace Funktionsplotter
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Effect Effect;

        Funktion3D Funktion;

        Matrix ViewMatrix;
        Matrix ProjectionMatrix;
        Matrix WorldMatrix;
        Forms.Form Form;

        public Game()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            Form = (Forms.Form)Forms.Form.FromHandle(Window.Handle);
            Form.FormBorderStyle = Forms.FormBorderStyle.None;
            var r = Forms.Screen.GetWorkingArea(Form);
            Form.Left = r.Left;
            Form.Top = r.Top;
            graphics.PreferredBackBufferWidth = r.Width;
            graphics.PreferredBackBufferHeight = r.Height;
            graphics.ApplyChanges();

            Input.Form = Form;
            Input.Exit += new ExitHandler(Input_Exit);


            //Funktion = new Funktion3D("max(0;10-((x+16)^2+y^2)/25)-min(2;(x+16)^2+y^2)/3+max(0;10-((x-16)^2+y^2)/25)-min(2;(x-16)^2+y^2)/3");
            //Funktion = new Funktion3D("sin(sqrt(x*x+y*y))");
            //Funktion = new Funktion3D("5 - ( sin(abs(x)) + cos(abs(y)) )");
            //Funktion = new Funktion3D("max(0 ; 10 - abs( max( abs(x); abs(y) ) ) )");
            //Funktion = new Funktion3D("max(sin(x);cos(y))");
            //Funktion = new Funktion3D("e^(pi*(sin(x+y)))");
            //Funktion = new Funktion3D("3*(x-1)^2");
            Funktion = new Funktion3D("e^(1/(y^2 + x^2 + 1))");

            base.Initialize();
        }

        void Input_Exit()
        {
            Exit();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            var r = Forms.Screen.GetWorkingArea(Form);
            Form.Left = r.Left;
            Form.Top = r.Top;

            int tmp = 1;

            Funktion.Generate(-tmp, tmp, -tmp, tmp, 128, Color.White);

            Effect = Content.Load<Effect>("Effect");
            spriteBatch = new SpriteBatch(GraphicsDevice);

            SetUpCamera();
        }

        private void SetUpCamera()
        {
            ViewMatrix = Matrix.CreateLookAt(new Vector3(0, 25, 15), new Vector3(0, 0, 0), new Vector3(0, 0, 1));
            ProjectionMatrix = Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver4, GraphicsDevice.Viewport.AspectRatio, 1.0f, 300.0f);
            WorldMatrix = Matrix.Identity;
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (Form.Focused)
            {
                Input.HandleKeyboardInput();
                Input.HandleMouseInput();

                WorldMatrix = Matrix.CreateRotationZ(Input.Rotation.Z) * Matrix.CreateRotationX(-Input.Rotation.X) * Matrix.CreateScale(Input.Scale);

                base.Update(gameTime);
            }
        }

        

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            RasterizerState rs = new RasterizerState();
            rs.FillMode = Input.FillMode;
            rs.CullMode = CullMode.None;
            GraphicsDevice.RasterizerState = rs;

            Effect.CurrentTechnique = Effect.Techniques["ColoredNoShading"];
            Effect.Parameters["xView"].SetValue(ViewMatrix);
            Effect.Parameters["xProjection"].SetValue(ProjectionMatrix);
            Effect.Parameters["xWorld"].SetValue(WorldMatrix);

            foreach (EffectPass pass in Effect.CurrentTechnique.Passes)
            {
                pass.Apply();
                Funktion.Draw(GraphicsDevice);
            }

            base.Draw(gameTime);
        }
    }
}
