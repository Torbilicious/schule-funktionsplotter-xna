﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;
using System.Diagnostics;

namespace Funktionsplotter
{
    static class Extensions
    {
        #region KeyboardState Extensions
        private static KeyboardState OldKeyboardState;
        public static bool IsKeyPressed(this KeyboardState ks, Keys key)
        {
            return ks.IsKeyDown(key) && OldKeyboardState.IsKeyUp(key);
        }
        public static bool IsKeyReleased(this KeyboardState ks, Keys key)
        {
            return ks.IsKeyUp(key) && OldKeyboardState.IsKeyDown(key);
        }
        /// <summary>
        /// Has to be called at the end of your Update method
        /// </summary>
        /// <param name="ks"></param>
        public static void Update(this KeyboardState ks)
        {
            OldKeyboardState = ks;
        }
        #endregion
        #region MouseState Extensions
        private static MouseState OldMouseState;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ms"></param>
        /// <param name="button">0: left Mousebutton
        /// 1: middle Mousebutton
        /// 2: right Mousebutton
        /// 3: XButton 1
        /// 4: XButton 2
        /// </param>
        /// <returns></returns>
        public static bool IsButtonPressed(this MouseState ms, int button)
        {
            ButtonState[] states = { ms.LeftButton, ms.MiddleButton, ms.RightButton, ms.XButton1, ms.XButton2 },
                oldStates = { OldMouseState.LeftButton, OldMouseState.MiddleButton, OldMouseState.RightButton, OldMouseState.XButton1, OldMouseState.XButton2 };
            return oldStates[button] != states[button] && states[button] == ButtonState.Pressed;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ms"></param>
        /// <param name="button">0: left Mousebutton
        /// 1: middle Mousebutton
        /// 2: right Mousebutton
        /// 3: XButton 1
        /// 4: XButton 2
        /// </param>
        /// <returns></returns>
        public static bool IsButtonReleased(this MouseState ms, int button)
        {
            ButtonState[] states = { ms.LeftButton, ms.MiddleButton, ms.RightButton, ms.XButton1, ms.XButton2 },
                oldStates = { OldMouseState.LeftButton, OldMouseState.MiddleButton, OldMouseState.RightButton, OldMouseState.XButton1, OldMouseState.XButton2 };
            return oldStates[button] != states[button] && states[button] == ButtonState.Released;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ms"></param>
        /// <param name="button">0: left Mousebutton
        /// 1: middle Mousebutton
        /// 2: right Mousebutton
        /// 3: XButton 1
        /// 4: XButton 2
        /// </param>
        /// <returns></returns>
        public static bool IsButtonDown(this MouseState ms, int button)
        {
            ButtonState[] states = { ms.LeftButton, ms.MiddleButton, ms.RightButton, ms.XButton1, ms.XButton2 };
            return states[button] == ButtonState.Pressed;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ms"></param>
        /// <param name="button">0: left Mousebutton
        /// 1: middle Mousebutton
        /// 2: right Mousebutton
        /// 3: XButton 1
        /// 4: XButton 2
        /// </param>
        /// <returns></returns>
        public static bool IsButtonUp(this MouseState ms, int button)
        {
            ButtonState[] states = { ms.LeftButton, ms.MiddleButton, ms.RightButton, ms.XButton1, ms.XButton2 };
            return states[button] == ButtonState.Released;
        }
        /// <summary>
        /// Has to be called at the end of your Update method
        /// </summary>
        /// <param name="ks"></param>
        public static void Update(this MouseState ms)
        {
            OldMouseState = ms;
        }

        public static Point Position(this MouseState ms)
        {
            return new Point(ms.X, ms.Y);
        }

        public static Point TrackMovement(this MouseState ms)
        {
            Point p = new Point(ms.X - OldMouseState.X - MovementRepositioningDifference.X, ms.Y - OldMouseState.Y - MovementRepositioningDifference.Y);

            if (MovementRepositioningDifference != Point.Zero)
                MovementRepositioningDifference = Point.Zero;

            return p;
        }

        private static Point MovementRepositioningDifference;
        public static void SetPosition(this MouseState ms, Point position)
        {
            MovementRepositioningDifference = new Point(position.X - ms.X, position.Y - ms.Y);
            Mouse.SetPosition(position.X, position.Y);
        }
        #endregion
    }
}
